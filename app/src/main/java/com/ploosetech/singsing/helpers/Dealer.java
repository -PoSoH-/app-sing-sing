package com.ploosetech.singsing.helpers;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by Sergiy Polishuk (PoSoH) on 05.08.2016.
 */
public class Dealer {

    public static final String NAVIGATION_BASE = "NAVIGATION.BASE";
    public static final String NAVIGATION_LEVEL = "NAVIGATION.LEVEL";
    public static final String NAVIGATION_DETAILS = "NAVIGATION.DETAILS";
    public static final String NAVIGATION_CONTENT = "NAVIGATION.CONTENT";
    public static final String NAVIGATION_LIKES = "NAVIGATION.LIKES";

    public static final String NAVIGATION_BASE_ENG_NAME = "NAVIGATION.BASE.ENG.NAME";
    public static final String NAVIGATION_BASE_ARB_NAME = "NAVIGATION.BASE.ARB.NAME";

    public static final String NAVIGATION_LEVEL_ENG_NAME = "NAVIGATION.LEVEL.ENG.NAME";
    public static final String NAVIGATION_LEVEL_ARB_NAME = "NAVIGATION.LEVEL.ARB.NAME";
    public static final String NAVIGATION_LEVEL_IMG_NAME = "NAVIGATION.LEVEL.IMG.NAME";

    public static final String FILENAME = "json_menu/json_menu.json";

    private static final String[] KEYS_COLLECTIONS = new String[] {
              "QUESTIONS", "NUMBERS"   , "LOCATIONS", "EMOTIONS", "DIRECTIONS"
            , "HOME"     , "RESTAURANT", "MAIL"     , "STREET"  , "UNIVERSITY"
            , "NATIONS", "RULERS", "ANTHEM", "CITIES", "DIALECT"
    };

    public static void hideKeyboard(final Activity activity){
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        View currentFocus = activity.getCurrentFocus();
        if (currentFocus != null) {
            IBinder iBinder = currentFocus.getWindowToken();
            if (iBinder != null) inputMethodManager.hideSoftInputFromWindow(iBinder, 0);
        }
    }

    public static void showKeyboard(final Activity activity){
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        View currentFocus = activity.getCurrentFocus();
        if (currentFocus != null) {
            IBinder iBinder = currentFocus.getWindowToken();
            if (iBinder != null) inputMethodManager.showSoftInputFromInputMethod(iBinder, 0);
        }
    }

    public static Bitmap readImageView(Context context, String address){
        InputStream stream = null;
        try {
            AssetManager manager = context.getAssets();
            stream = manager.open(address);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return BitmapFactory.decodeStream(stream);
    }

    public static void collectionSearch(Context context
            , final String searchString
            , OnResultSearch result){

        ArrayList<String> resultEngCollection = new ArrayList<>();
        ArrayList<String> resultArbCollection = new ArrayList<>();
        ArrayList<String> resultUAECollection = new ArrayList<>();
        ArrayList<String> resultVidCollection = new ArrayList<>();

        ArrayList<String> fullEngCollection = new ArrayList<>();
        ArrayList<String> fullArbCollection = new ArrayList<>();
        ArrayList<String> fullUaeCollection = new ArrayList<>();
        ArrayList<String> fullVidCollection = new ArrayList<>();

        InputStream inputStream;
        AssetManager am = context.getAssets();
        String JSONResult = null;

        // load file json menu

        try {
            inputStream = am.open(FILENAME);
            if(inputStream != null) {
                InputStreamReader reader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(reader);
                String readerResult;
                StringBuilder builder = new StringBuilder();
                while ((readerResult = bufferedReader.readLine()) != null) {
                    builder.append(readerResult);
                }
                JSONResult = builder.toString();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        //parse file json

        JSONObject menuSelected;
        try {
            menuSelected = new JSONObject(JSONResult);
            JSONArray engText;
            JSONArray arbText;
            JSONArray uaeText;
            JSONArray vidText;
//            int length = menuSelected.length();
            int length = KEYS_COLLECTIONS.length;
            for(int pos = 0; pos<length; pos++) {
                engText = menuSelected.getJSONObject(KEYS_COLLECTIONS[pos]).getJSONArray("ENG");
                arbText = menuSelected.getJSONObject(KEYS_COLLECTIONS[pos]).getJSONArray("ARB");
                uaeText = menuSelected.getJSONObject(KEYS_COLLECTIONS[pos]).getJSONArray("UAE");
                vidText = menuSelected.getJSONObject(KEYS_COLLECTIONS[pos]).getJSONArray("VIDEO");
                for(int item=0, len=engText.length();item<len;item++){
                    fullEngCollection.add(engText.getString(item));
                }
                for(int item=0, len=arbText.length();item<len;item++){
                    fullArbCollection.add(arbText.getString(item));
                }
                for(int item=0, len=uaeText.length();item<len;item++){
                    fullUaeCollection.add(uaeText.getString(item));
                }
                for(int item=0, len=vidText.length();item<len;item++){
                    fullVidCollection.add(vidText.getString(item));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(searchString != null) {
            for (int pos = 0, len = fullEngCollection.size(); pos < len; pos++) {
                int countSymbol = searchString.length();
                StringBuilder temp = new StringBuilder();
                for (int p = 0; p < countSymbol; p++) {
                    Dealer.LOG("Count symbols => " + searchString.length());
                    Dealer.LOG("Count size symbols => " + fullEngCollection.get(pos).length());
                    if(fullEngCollection.get(pos).length() >= countSymbol){
                        temp.append(fullEngCollection.get(pos).toLowerCase().charAt(p));
                        if (temp.toString().toLowerCase().equals(searchString)) {
                            resultEngCollection.add(fullEngCollection.get(pos));
                            resultArbCollection.add(fullArbCollection.get(pos));
                            resultUAECollection.add(fullUaeCollection.get(pos));
                            resultVidCollection.add(fullVidCollection.get(pos));
                        }
                    }
                }
            }
        }else{
            resultEngCollection = fullEngCollection; //.add(fullEngCollection.get(pos));
            resultArbCollection = fullArbCollection; //.add(fullArbCollection.get(pos));
            resultUAECollection = fullUaeCollection; //.add(fullUaeCollection.get(pos));
            resultVidCollection = fullVidCollection; //.add(fullVidCollection.get(pos));
        }

        result.resultSuccess(resultEngCollection, resultArbCollection
                , resultUAECollection, resultVidCollection);
    }

    public static void LOG(String values){
        Log.e("RAMSAH: ", "RAPORT -> " + values + " ! ");
    }

//    public static HashMap<String, String> parseJSONObject(final String KEY, String jsonValue){
//        HashMap<String, String> parseResult = new HashMap<>();
//
//        String listItemMenuEng[] = null;
//        String listItemMenuArabic[] = null;
//        String listItemMenuIcons[] = null;
//        String listItemMenuKeys[] = null;
//
//        JSONObject menuSelected;
//        try {
//            menuSelected = new JSONObject(jsonValue);
//            JSONArray english = menuSelected.getJSONObject(KEY).getJSONArray("E");
//            JSONArray arabic = menuSelected.getJSONObject(KEY).getJSONArray("A");
//            JSONArray images = menuSelected.getJSONObject(KEY).getJSONArray("IMAGES");
//            JSONArray keys = menuSelected.getJSONObject(KEY).getJSONArray("KEYS");
//
//            listItemMenuEng = new String[english.length()];
//            listItemMenuArabic = new String[arabic.length()];
//            listItemMenuIcons = new String[images.length()];
//            listItemMenuKeys = new String[keys.length()];
//
//            for(int pos=0, l=english.length();pos<l;pos++){
//                listItemMenuEng[pos] = english.getString(pos);
//            }
//            for(int pos=0, l=arabic.length();pos<l;pos++){
//                listItemMenuArabic[pos] = arabic.getString(pos);
//            }
//            for(int pos=0, l=images.length();pos<l;pos++){
//                listItemMenuIcons[pos] = images.getString(pos);
//            }
//            for(int pos=0, l=keys.length();pos<l;pos++){
//                listItemMenuKeys[pos] = arabic.getString(pos);
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        parseResult.put("NAME_ENG", listItemMenuArabic);
//        parseResult.put("NAME_ARB", );
//        parseResult.put("NAME_IMG", );
//        parseResult.put("NAME_KEY", );
//
//        return parseResult.size()>0?parseResult:null;
//    }

    public interface OnResultSearch{
        void resultSuccess(ArrayList<String> engResult, ArrayList<String> arbResult, ArrayList<String> uaeResult, ArrayList<String> videoResult);
    }

}

