package com.ploosetech.singsing.helpers;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by Sergiy Polishuk (PoSoH) on 05.08.2016.
 */
public class Preferences {

    /// methods and class for write

    public static final boolean TYPE_LIKE = true;
    public static final boolean TYPE_SAVE = false;

    public static void saveStringInformation(Context context, final String KEY, final String VALUE){
        ProgramSettings.saveStringInformation(context, KEY, VALUE);
    }

    public static String readStringInformation(Context context, final String KEY){
        return ProgramSettings.readStringInformation(context, KEY);
    }

    public static void removeStringInformation(Context context, final String KEY){
        ProgramSettings.removeStringInformation(context, KEY);
    }

    public static void saveListInformation(Context context, final boolean KEY, final String VALUE){
        ProgramSettings.saveListInformation(context, KEY, VALUE);
    }

    public static ArrayList<String> readListInformation(Context context, final boolean KEY){
        return ProgramSettings.readListInformation(context, KEY);
    }

    private static class ProgramSettings{
        public static final String PREFERENCES = "com.ploosetech.signsign.program.settings.sign.sign";


        private static SharedPreferences getPreferences(final Context context){
            return context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
        }

        public static void saveStringInformation(Context context, final String KEY, final String VALUE){
            SharedPreferences preferences = getPreferences(context);
            SharedPreferences.Editor edit = preferences.edit();
            edit.putString(KEY, VALUE);
            edit.apply();
        }

        public static void saveListInformation(Context context, final boolean KEY, final String VALUE){
            boolean isSizes = false;
            SharedPreferences preferences = getPreferences(context);
            SharedPreferences.Editor edit = preferences.edit();
            JSONArray jsonArray = null;
            String ListJson = null;
            try {
                if (KEY) {
                    ListJson = preferences.getString("PREFERENCES_LIKES_SAVED", null);
                } else {
                    ListJson = preferences.getString("PREFERENCES_SAVED_INFO", null);
                }

                if(ListJson != null){
                    jsonArray = new JSONArray(ListJson);
                }else{
                    jsonArray = new JSONArray();
                }

                for(int pos=0, len = jsonArray.length();pos<len; pos++) {
                    if (jsonArray.getString(pos).equals(VALUE)){
                        jsonArray.remove(pos);
                        isSizes = true;
                        break;
                    }
                }

                if(!isSizes)
                    jsonArray.put(VALUE);

                if (KEY) {
                    edit.putString("PREFERENCES_LIKES_SAVED", jsonArray.toString());
                }else {
                    edit.putString("PREFERENCES_SAVED_INFO", jsonArray.toString());
                }
            }catch (JSONException ex){
                Dealer.LOG(" RAPPORT -> " + ex.getMessage());
            }
            edit.apply();
        }

        public static ArrayList<String> readListInformation(Context context, final boolean KEY){
            SharedPreferences preferences = getPreferences(context);
            SharedPreferences.Editor edit = preferences.edit();
            ArrayList<String> temp = new ArrayList<>();
            JSONArray jsonArray = null;
            String listJson = null;
            try {
                if(KEY)
                    listJson = preferences.getString("PREFERENCES_LIKES_SAVED", null);
                else
                    listJson = preferences.getString("PREFERENCES_SAVED_INFO", null);

                if(listJson !=null)
                    jsonArray = new JSONArray(listJson);
                else
                    jsonArray = new JSONArray();

                for(int pos=0, len = jsonArray.length();pos<len; pos++) {
                    temp.add(jsonArray.getString(pos));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return temp;
        }

        public static String readStringInformation(Context context, final String KEY){
            SharedPreferences preferences = getPreferences(context);
            return preferences.getString(KEY, null);
        }

        public static void removeStringInformation(Context context, final String KEY){
            SharedPreferences preferences = getPreferences(context);
            SharedPreferences.Editor edit = preferences.edit();
            edit.remove(KEY);
            edit.apply();
        }
    }

}
