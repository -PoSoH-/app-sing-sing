package com.ploosetech.singsing.helpers;

import android.content.Context;
import android.view.animation.AnimationUtils;

import com.ploosetech.singsing.R;

/**
 * Created by Sergiy Polishuk (PoSoH) on 18.07.2016.
 */
public class Animation {

    public static final int OLD_TO_RIGHT = -2;
    public static final int OLD_TO_LEFT = -4;
    public static final int NEW_TO_RIGHT = -3;
    public static final int NEW_TO_LEFT = -5;

    public static android.view.animation.Animation getTypeAnimation(Context context, int realization){
        android.view.animation.Animation animation = null;
        if(realization == OLD_TO_LEFT){
            animation = AnimationUtils.loadAnimation(context, R.anim.slide_right_to_left_old_fr);
            return animation;
        }
        if(realization == OLD_TO_RIGHT){
            animation = AnimationUtils.loadAnimation(context, R.anim.slide_left_to_right_old_fr);
            return animation;
        }
        if(realization == NEW_TO_RIGHT){
            animation = AnimationUtils.loadAnimation(context, R.anim.slide_left_to_rigth_new_fr);
            return animation;
        }
        if(realization == NEW_TO_LEFT){
            animation = AnimationUtils.loadAnimation(context, R.anim.slide_right_to_left_new_fr);
            return animation;
        }
        return null;
    }
}
