package com.ploosetech.singsing.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TextView;

import com.ploosetech.singsing.R;

/**
 * Created by PoSoH on 31.03.2016.
 */
public class TextViewGE extends android.support.v7.widget.AppCompatTextView {
    public TextViewGE(Context context) {
        super(context);
    }

    public TextViewGE(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context, attrs);
    }

    public TextViewGE(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setCustomFont(context, attrs);
    }

    private void setCustomFont(Context ctx, AttributeSet attrs) {
        TypedArray a = ctx.obtainStyledAttributes(attrs, R.styleable.TextViewGE);
        String customFont = a.getString(R.styleable.TextViewGE_GE_Flow_Bold);
        setCustomFont(ctx, customFont);
        a.recycle();
    }

    public boolean setCustomFont(Context ctx, String asset) {
        Typeface tf = null;
        try {
//            tf = Typeface.createFromAsset(ctx.getAssets(), asset);
            tf = Typeface.createFromAsset(ctx.getAssets(),"fonts/GE_Flow_Bold.otf");
            setTypeface(tf);
        } catch (Exception e) {
            Log.d("TYPE FACE", "Could not get typeface: "+e.getMessage());
            return false;
        }
        return true;
    }
}
