package com.ploosetech.singsing.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.ploosetech.singsing.R;
import com.ploosetech.singsing.activites.Main;
import com.ploosetech.singsing.helpers.Dealer;
import com.ploosetech.singsing.helpers.Preferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Serhii Polishchuk (PoSoH)
 */
public class ContentMenu extends Fragment {
//        implements SurfaceHolder.Callback, MediaPlayer.OnPreparedListener {

    private OnContentMenuListener mListener;

    private final int mButtonCount = 7;
    private final int mTextCount = 3;

    private final String RESOURCE = "android.resource://";
    private final String OUT_DIRECTORY = "Ramsah";

    private ImageView[] mButtonCollections;
    private TextView[] mTextTableCollections;

    private VideoView mVideoView;
    private boolean mVideoRepeat = false;
    private int mProgressPosition;
    private int mCurrentViewPosition = -2;
    private boolean mCurrentSave = false;
    private ProgressDialog progressDialog;
    private MediaController mediaControls;

    private SurfaceView vidSurface;
    private MediaPlayer mediaPlayer;

    private List<String> englishTexts = new ArrayList<>();
    private List<String> arabicTexts = new ArrayList<>();
    private List<String> UAETexts = new ArrayList<>();
    private List<String> mVideoPositionResourses = new ArrayList<>();

    private int imageWidth;
    private int mResource[] = new int[]{
            R.raw.video_file_248,
            R.raw.video_file_247,
            R.raw.video_file_245,
            R.raw.video_file_236,
            R.raw.video_file_144,
            R.raw.video_file_254,
            R.raw.video_file_447,
            R.raw.video_file_449,
            R.raw.video_file_465,
            R.raw.video_file_478,
            R.raw.video_file_554,
            R.raw.video_file_665,
            R.raw.video_file_745,
            R.raw.video_file_965,
            R.raw.video_file_966,
            R.raw.video_file_976,
            R.raw.video_file_977,
            R.raw.video_file_988
    };

    private HashMap<String, ArrayList<String>> mInformation = new HashMap<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layoutFragment = inflater.inflate(R.layout.view_frg_content, container, false);
        parseJSONObject();
        initializeFragmentView(layoutFragment);
        return layoutFragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        assert mListener !=null;

        Display display = ((WindowManager) getActivity()
                .getSystemService(AppCompatActivity.WINDOW_SERVICE)).getDefaultDisplay();

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        imageWidth = displayMetrics.widthPixels;


        RelativeLayout frame = (RelativeLayout) view.findViewById(R.id.contentVideoContainer);
//        frame.getLayoutParams().width = display.getWidth() - 2*10;
//        frame.getLayoutParams().height = display.getWidth() - 2*20 - 2*30;

        if (mediaControls == null)
            mediaControls = new MediaController(getContext());

//        vidSurface = (SurfaceView) view.findViewById(R.id.contentSurfaceView);
//        SurfaceHolder vidHolder = vidSurface.getHolder();
//        vidHolder.addCallback(this);
//
//        SurfaceHolder vidHolder;
//        SurfaceView vidSurface;
//        vidSurface = (SurfaceView) view.findViewById(R.id.contentSurfaceView);
//        vidHolder = vidSurface.getHolder();
//        vidHolder.addCallback(this);
//        String vidAddress = "https://archive.org/download/ksnn_compilation_master_the_internet/ksnn_compilation_master_the_internet_512kb.mp4";

        FrameLayout layout = (FrameLayout) view.findViewById(R.id.contentVideoViewContainer);

        mVideoView = (VideoView) view.findViewById(R.id.contentVideoView);

        int sizeScreenWidth = (imageWidth - 30*2 - 20*2) / 1;

        mVideoView.getLayoutParams().width = sizeScreenWidth;
        mVideoView.getLayoutParams().height = sizeScreenWidth * 4 / 3;
        mVideoView.setMediaController(mediaControls);

        videoStart();

        mCurrentViewPosition = mListener.onListenerActivePosition();
        changeSelectedText();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof OnContentMenuListener)
            mListener = (OnContentMenuListener) context;
        else
            throw new ClassCastException("Error!. Please implements OnContentMenuListener interface");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void videoStart(){
        try {
            mVideoView.setVideoURI(Uri.parse(RESOURCE + getContext().getPackageName() + "/" + mResource[Integer
                    .valueOf(mVideoPositionResourses.get(mCurrentViewPosition))]));
            mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    if(mVideoRepeat) mediaPlayer.start();
                    else mButtonCollections[2].setImageDrawable(getResources().getDrawable(R.drawable.button_content_current_play));
                }
            });

        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }

        mVideoView.requestFocus();
        mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mediaPlayer) {

            }
        });
    }

    private void parseJSONObject(){

        String KEY = Preferences.readStringInformation(getContext(), Dealer.NAVIGATION_CONTENT);
        JSONObject menuSelected;
        String JSON = null; //getString(R.string.levelDetails);

        if(mListener!=null){
            mInformation = mListener.onListenerSearchResult();
        }

        if(mInformation!=null && mInformation.size()>0){
            englishTexts = mInformation.get(Main.TYPE_TEXT_ENG);
            arabicTexts = mInformation.get(Main.TYPE_TEXT_ARB);
            UAETexts = mInformation.get(Main.TYPE_TEXT_UAE);
            mVideoPositionResourses = mInformation.get(Main.TYPE_TEXT_VID);
        }else {
            try {
                menuSelected = new JSONObject(JSON);
                JSONArray english = menuSelected.getJSONObject(KEY).getJSONArray("ENG");
                JSONArray arabic = menuSelected.getJSONObject(KEY).getJSONArray("ARB");
                JSONArray uaeTexts = menuSelected.getJSONObject(KEY).getJSONArray("UAE");
                JSONArray vidTexts = menuSelected.getJSONObject(KEY).getJSONArray("VIDEO");
                for (int pos = 0, l = english.length(); pos < l; pos++) {
                    englishTexts.add(english.getString(pos));
                }
                for (int pos = 0, l = arabic.length(); pos < l; pos++) {
                    arabicTexts.add(arabic.getString(pos));
                }
                for (int pos = 0, l = uaeTexts.length(); pos < l; pos++) {
                    UAETexts.add(uaeTexts.getString(pos));
                }
                for (int pos = 0, l = vidTexts.length(); pos < l; pos++) {
                    mVideoPositionResourses.add(vidTexts.getString(pos));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void initializeFragmentView(View view){

        mButtonCollections = new ImageView[mButtonCount];
        mButtonCollections[0] = (ImageView) view.findViewById(R.id.toolbarBackButton);
        mButtonCollections[1] = (ImageView) view.findViewById(R.id.contentPreviewButton);
        mButtonCollections[2] = (ImageView) view.findViewById(R.id.contentPlayViewButton);
        mButtonCollections[3] = (ImageView) view.findViewById(R.id.contentCircleViewButton);
        mButtonCollections[4] = (ImageView) view.findViewById(R.id.contentNextViewButton);
        mButtonCollections[5] = (ImageView) view.findViewById(R.id.contentButtonSave);
        mButtonCollections[6] = (ImageView) view.findViewById(R.id.contentButtonLike);

        mTextTableCollections = new TextView[mTextCount];
        mTextTableCollections[0] = (TextView) view.findViewById(R.id.contentRowOneColTwoValues);    // arabic text
        mTextTableCollections[1] = (TextView) view.findViewById(R.id.contentRowTwoColTwoValues);    // english text
        mTextTableCollections[2] = (TextView) view.findViewById(R.id.contentRowThreeColTwoValues);  // UAE text

        changeButtonState();

        mButtonCollections[0].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mListener!=null){
                    if(mListener.onListenerSearches())
                        mListener.onStartMenuItemListener(Main.TYPE_BASICS);
                    else
                        mListener.onStartMenuItemListener(Main.TYPE_DETAILS);
                }
            }
        });


        mButtonCollections[1].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dealer.LOG("PREVIEW BUTTON CLICK");
                if(mCurrentViewPosition == 0){
                    mCurrentViewPosition = arabicTexts.size()-1;
                }else{
                    mCurrentViewPosition--;
                }
                changeSelectedText();
            }
        });

        mButtonCollections[2].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dealer.LOG("PLAY BUTTON CLICK");

//                if(mediaPlayer.isPlaying()){
//                    mediaPlayer.pause();
//                    mButtonCollections[2].setImageDrawable(getResources().getDrawable(R.drawable.button_content_current_play));
//                }else{
//                    mediaPlayer.start();
//                    mButtonCollections[2].setImageDrawable(getResources().getDrawable(R.drawable.button_content_current_play_active));
//                }

                if(mVideoView.isPlaying()){
                    mVideoView.pause();
                    mButtonCollections[2].setImageDrawable(getResources().getDrawable(R.drawable.button_content_current_play));
                }else{
                    mVideoView.start();
                    mButtonCollections[2].setImageDrawable(getResources().getDrawable(R.drawable.button_content_current_play_active));
                }

            }
        });

        mButtonCollections[3].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dealer.LOG("CYCLE PLAY BUTTON CLICK");
                if(mVideoRepeat) mVideoRepeat = false;
                else mVideoRepeat = true;
                changeButtonState();
            }
        });

        mButtonCollections[4].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dealer.LOG("NEXT VIEW BUTTON CLICK");
                if(mCurrentViewPosition==arabicTexts.size()-1){
                    mCurrentViewPosition=0;
                }else{
                    mCurrentViewPosition++;
                }
                changeSelectedText();
            }
        });

        mButtonCollections[5].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dealer.LOG("SAVE BUTTON CLICK");
                if(!mCurrentSave) {

                    File file = getPathDir(OUT_DIRECTORY);//+"/"+englishTexts.get(mCurrentViewPosition) + ".mp4");

                    if(isExternalStorageWritable()){

//                        FileOutputStream outputStream;

                        try {

                            InputStream ins = getResources().openRawResource(mResource[Integer
                                        .valueOf(mVideoPositionResourses.get(mCurrentViewPosition))]);
                            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                            int size = 0;
                            byte[] buffer = new byte[1024];
                            while((size=ins.read(buffer,0,1024))>=0){
                                outputStream.write(buffer,0,size);
                            }
                            ins.close();
                            buffer=outputStream.toByteArray();
                            FileOutputStream fos = new FileOutputStream(file.getAbsolutePath()
                                    + "/"
                                    + englishTexts.get(mCurrentViewPosition)
                                    .replace(" ", "_")
                                    .replace("?", "")
                                    .replace("!", "")
                                    + ".mp4"); //getContext().openFileOutput(file.getAbsolutePath() + "/" + englishTexts.get(mCurrentViewPosition)));
                            fos.write(buffer);
                            fos.close();

                            Preferences.saveListInformation(getContext()
                                    , Preferences.TYPE_SAVE
                                    , englishTexts.get(mCurrentViewPosition));

                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(getContext(), "ERROR SAVE FILE"
                                    + "\n"
                                    + e.getMessage() , Toast.LENGTH_SHORT).show();
                        }
                        mButtonCollections[5].setImageDrawable(getResources().getDrawable(R.drawable.button_content_save_active));
                    } else {
                        Toast.makeText(getContext(), "ERROR SAVE FILE", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        mButtonCollections[6].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dealer.LOG("LIKE VIEW BUTTON CLICK");
                Preferences.saveListInformation(getContext()
                        , Preferences.TYPE_LIKE
                        , englishTexts.get(mCurrentViewPosition));
                changeShowFavorites();
            }
        });
    }

    private void changeButtonState(){
        if(mVideoRepeat){
            mButtonCollections[3].setImageDrawable(getResources().getDrawable(R.drawable.button_content_repeat_play_active));
        }else{
            mButtonCollections[3].setImageDrawable(getResources().getDrawable(R.drawable.button_content_repeat_play));
        }
    }

    private void changeSelectedText(){

        changeShowFavorites();
        changeShowSaved();

        mTextTableCollections[0].setText(arabicTexts.get(mCurrentViewPosition));     // arabic text
        mTextTableCollections[1].setText(englishTexts.get(mCurrentViewPosition));    // english text
        mTextTableCollections[2].setText(UAETexts.get(mCurrentViewPosition));        // UAE text

        videoStart();
    }

    private void changeShowFavorites(){
        mButtonCollections[6].setImageDrawable(getResources().getDrawable(R.drawable.button_content_register_favorite));
        ArrayList<String> isLikes = Preferences.readListInformation(getContext(), Preferences.TYPE_LIKE);
        for(String like : isLikes){
            if(englishTexts.get(mCurrentViewPosition).equals(like)){
                mButtonCollections[6].setImageDrawable(getResources().getDrawable(R.drawable.button_content_remove_favorite));
                return;
            }
        }
    }

    private void changeShowSaved(){
        mButtonCollections[5].setImageDrawable(getResources().getDrawable(R.drawable.button_content_save));
        ArrayList<String> isSave = Preferences.readListInformation(getContext(), Preferences.TYPE_SAVE);
        for(String save : isSave){
            if(englishTexts.get(mCurrentViewPosition).equals(save)){
                mButtonCollections[5].setImageDrawable(getResources().getDrawable(R.drawable.button_content_save_active));
                mCurrentSave = true;
                return;
            }
        }
    }

    private File getPathDir(String directoryName){
        // Get the directory for the user's public pictures directory.
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_MOVIES), directoryName);
        if (!file.mkdirs()) {
            Dealer.LOG("Directory not created");
        }
        Dealer.LOG("Directory is to created : " + file.getAbsolutePath());
        return file;
    }

    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }


    public static ContentMenu createFragment() {return new ContentMenu();}

//    @Override
//    public void surfaceCreated(SurfaceHolder surfaceHolder) {
//
//        surfaceHolder.addCallback(this);
//        String vidAddress = "https://archive.org/download/ksnn_compilation_master_the_internet/ksnn_compilation_master_the_internet_512kb.mp4";
//
//        try {
//            InputStream stream = null;
//            AssetManager manager = null;
//            AssetFileDescriptor descriptor;
//
//            mediaPlayer = new MediaPlayer();
////            manager = getContext().getAssets();
//            descriptor = getContext().getAssets().openFd("video/file.mp4");
//            mediaPlayer.setDataSource(descriptor.getFileDescriptor());
//            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
//            mediaPlayer.setOnPreparedListener(this);
//
//            Dealer.LOG("ACCESS");
//
//            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//                @Override
//                public void onCompletion(MediaPlayer mediaPlayer) {
//                    if(mVideoRepeat) mediaPlayer.start();
//                    else mButtonCollections[2].setImageDrawable(getResources().getDrawable(R.drawable.button_content_current_play));
//                }
//            });
//
//            mediaPlayer.setDisplay(surfaceHolder);
//            mediaPlayer.prepareAsync();
//
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//            Log.e("Error", e.getMessage());
//        } catch (IOException e) {
//            e.printStackTrace();
//            Log.e("Error", e.getMessage());
//        }
//    }
//
//    @Override
//    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
//        Dealer.LOG("RAPPORT => " + surfaceHolder.getSurface().describeContents() + i);
//    }
//
//    @Override
//    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
//        mediaPlayer = null;
//    }
//
//    @Override
//    public void onPrepared(MediaPlayer mediaPlayer) {
//        mediaPlayer.start();
//    }

    public interface OnContentMenuListener {
        void onStartMenuItemListener(final int fragmentType);
        HashMap onListenerSearchResult ();
        boolean onListenerSearches();
        int onListenerActivePosition();
    }
}
