package com.ploosetech.singsing.fragments;

import android.content.Context;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.ploosetech.singsing.R;
import com.ploosetech.singsing.activites.Main;
import com.ploosetech.singsing.adapters.DetailsMenuAdapter;
import com.ploosetech.singsing.helpers.Dealer;
import com.ploosetech.singsing.helpers.Preferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Serhii Polishchuk (PoSoH)
 */
public class DetailsMenu extends Fragment {

    private OnDetailsMenuListener mListener;

    private ListView mListViewer;

    private TextView mViewArabikLabel;
    private TextView mViewEnglishLabel;
    private ImageView mItemSelectedView;
    private View mLevelAboutContiner;
    private WebView mSiteOpenContainer;

    private String mIcon;
    private String mTextBases;
    private String mTextFat;
    private String mTextFooter;
    private String mLinkURL;

    private ImageView levelAboutIcon;
    private WebView levelAboutBaseContent;
    private TextView txtSiteOpenContainer;
    private TextView fatText;
    private TextView footerText;
    private TextView urlLink;

    private String KEY;

    private int mAdvanceMenuSelectedItem = -1;
    private int mPosition = -1;

    private HashMap<String, ArrayList<String>> information = new HashMap<String, ArrayList<String>>();

    private ArrayList<String> englishTexts = new ArrayList<>();
    private ArrayList<String> arabicTexts = new ArrayList<>();
    private ArrayList<String> uaeTexts = new ArrayList<>();
    private ArrayList<String> vidTexts = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layoutFragment = inflater.inflate(R.layout.view_frg_levels, container, false);
        parseJSONObject();
        initializeFragmentView(layoutFragment);
        return layoutFragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        DetailsMenuAdapter adapter = new DetailsMenuAdapter(getContext(), englishTexts, arabicTexts);
        mListViewer.setAdapter(adapter);
        mListViewer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Preferences.saveStringInformation(getContext(), Dealer.NAVIGATION_CONTENT, String.valueOf(i));
                assert mListener != null;
                mListener.onStartMenuItemListener(Main.TYPE_CONTENT);
                mListener.onListenerSaveActivePosition(i);
            }
        });

        switch (KEY) {
            case "IDEA":
            case "VOLUNTEERS":
            case "EMIRATES":
            case "HUMAN":
            case "SUPPORT":

                String mTextPrototype = "<html>" +
                    "<head>" +
//                        "<style>" +
//                            "@font-face{" +
//                                "font-family:\"ARABIC\"; " +
//                                "src : url('file:///android_asset/fonts/GE_Flow_Bold.otf')" +
//                                "font-weight: normal;" +
//                                "font-style: normal;" +
//                                "} " +
//                            "@font-face{" +
//                                "font-family:\"ARABIC-BOLD\"; " +
//                                "src : url('file:///android_asset/fonts/GE_Flow_Bold.otf')" +
//                                "font-weight: bold;" +
//                                "font-style: normal;" +
//                            "} " +
//                            "text-align:justify;color:#000000;" +
//                            "P{font-family : \"ARABIC\"}"+
//                        "</style>" +
                        "</head>" +
                    "<body " +
                        "style=\" " +
//                            "@font-face{font-family:\"ARABIC\"; " +
//                            "src : url('file:///android_asset/fonts/GE_Flow_Bold.otf');}" +
                            "text-align:justify;" +
                            "color:#000000;\"" +
                        "> " +
                        "<P style=\"font-family : \"ARABIC\" \">"
                        + "$"
                        + "</P><P style=\"font-family : \"ARABIC-BOLD\" \">"
                        + "#"
                        + "</P>" +
                    "</body>" +
                    "</html>";

                String forWebViewOne = mTextPrototype.replace("$", mTextBases);
                String forWebViewTwo = forWebViewOne.replace("#", mTextFat);
//                levelAboutBaseContent.loadData(forWebViewTwo, "text/html", "UTF-8");
                switch (KEY) {
                    case "IDEA":
                    case "SUPPORT":
                        levelAboutIcon.setImageDrawable(getResources().getDrawable(R.drawable.idea_support));
                        if(KEY.equals("IDEA")){
                            txtSiteOpenContainer.setText(Html.fromHtml(getResources().getString(R.string.about_idea_section_text)));
                        }else{
                            txtSiteOpenContainer.setText(Html.fromHtml(getResources().getString(R.string.about_support_section_text)));
                        }
                        break;
                    case "VOLUNTEERS":
                        levelAboutIcon.setImageDrawable(getResources().getDrawable(R.drawable.volunteers));
                        txtSiteOpenContainer.setText(Html.fromHtml(getResources().getString(R.string.about_volunteers_section_text)));
                        break;
                    case "EMIRATES":
                        levelAboutIcon.setImageDrawable(getResources().getDrawable(R.drawable.foundation));
                        txtSiteOpenContainer.setText(Html.fromHtml(getResources().getString(R.string.about_foundations_section_text)));
                        break;
                    case "HUMAN":
                        levelAboutIcon.setImageDrawable(getResources().getDrawable(R.drawable.humanitarian));
                        txtSiteOpenContainer.setText(Html.fromHtml(getResources().getString(R.string.about_humanitarian_section_text)));
                        break;
                }
//                footerText.setText(mTextFooter);
                urlLink.setText(mLinkURL);
                urlLink.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        assert mListener != null;
                        mListener.onListenerDialogStart();

//                        mLevelAboutContiner.setVisibility(View.GONE);
//                        mSiteOpenContainer.setVisibility(View.VISIBLE);
//                        mSiteOpenContainer.getSettings().setJavaScriptEnabled(true);
//                        mSiteOpenContainer.loadUrl("http://" + mLinkURL);
                    }
                });
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof OnDetailsMenuListener)
            mListener = (OnDetailsMenuListener) context;
        else
            throw new ClassCastException("Error!. Please implements OnDetailsMenuListener interface");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void parseJSONObject(){
        KEY = Preferences.readStringInformation(getContext(), Dealer.NAVIGATION_DETAILS);
        JSONObject menuSelected = null;

        AssetManager am = getContext().getAssets();
        InputStream inputStream = null;
        String JSONResult = null; //getString(R.string.levelDetails);

        try {
            inputStream = am.open(Dealer.FILENAME);
            if(inputStream != null) {
                InputStreamReader reader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(reader);
                String readerResult;
                StringBuilder builder = new StringBuilder();
                while ((readerResult = bufferedReader.readLine()) != null) {
                    builder.append(readerResult);
                }
                JSONResult = builder.toString();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        switch (KEY){
            case "IDEA":
            case "VOLUNTEERS":
            case "EMIRATES":
            case "HUMAN":
            case "SUPPORT":
                try {
                    menuSelected = new JSONObject(JSONResult);
                    JSONObject temp = new JSONObject(menuSelected.getJSONObject(KEY).toString());
                    mIcon = temp.getString("IMAGE");
                    mTextBases = temp.getString("TEXT");
                    mTextFat = temp.getString("TEXT_FAT");
                    mTextFooter = temp.getString("FOOTER");
                    mLinkURL = temp.getString("URL");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            default:
                try {
                    if(mListener!=null){
                        if(Preferences.readStringInformation(getContext()
                                , Dealer.NAVIGATION_LEVEL_ENG_NAME).equals(Main.LIKES)) {
                            assert mListener != null;
                            information = mListener.onListenerSearchResult();
                            englishTexts = information.get(Main.TYPE_TEXT_ENG);
                            arabicTexts = information.get(Main.TYPE_TEXT_ARB);
                            uaeTexts = information.get(Main.TYPE_TEXT_UAE);
                            vidTexts = information.get(Main.TYPE_TEXT_VID);
                        }else {
                            menuSelected = new JSONObject(JSONResult);
                            JSONArray english = menuSelected.getJSONObject(KEY).getJSONArray("ENG");
                            JSONArray arabic = menuSelected.getJSONObject(KEY).getJSONArray("ARB");
                            JSONArray uae = menuSelected.getJSONObject(KEY).getJSONArray("UAE");
                            JSONArray video = menuSelected.getJSONObject(KEY).getJSONArray("VIDEO");
                            for (int pos = 0, l = english.length(); pos < l; pos++) {
                                englishTexts.add(english.getString(pos));
                            }
                            for (int pos = 0, l = arabic.length(); pos < l; pos++) {
                                arabicTexts.add(arabic.getString(pos));
                            }
                            for (int pos = 0, l = uae.length(); pos < l; pos++) {
                                uaeTexts.add(uae.getString(pos));
                            }
                            for (int pos = 0, l = video.length(); pos < l; pos++) {
                                vidTexts.add(video.getString(pos));
                            }

                            information.put(Main.TYPE_TEXT_ENG, englishTexts);
                            information.put(Main.TYPE_TEXT_ARB, arabicTexts);
                            information.put(Main.TYPE_TEXT_UAE, uaeTexts);
                            information.put(Main.TYPE_TEXT_VID, vidTexts);

                            assert mListener != null;
                            mListener.onListenerSearchSaving(information
                                    , null);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
        }
    }

    private void initializeFragmentView(View view){

        switch (KEY) {
            case "IDEA":
            case "VOLUNTEERS":
            case "EMIRATES":
            case "HUMAN":
            case "SUPPORT":
                mListViewer = (ListView) view.findViewById(R.id.fragmentLevelListView);
                mLevelAboutContiner = view.findViewById(R.id.levelContainerAbout);
                mSiteOpenContainer = (WebView) view.findViewById(R.id.levelWebView);
                mListViewer.setVisibility(View.GONE);
                mLevelAboutContiner.setVisibility(View.VISIBLE);

                levelAboutIcon = (ImageView) view.findViewById(R.id.levelIconAboutMenuImage);
                levelAboutIcon.setImageBitmap(Dealer.readImageView(getContext(), mIcon));
//                levelAboutBaseContent = (WebView) view.findViewById(R.id.levelWebViewContainer);
                txtSiteOpenContainer = (TextView) view.findViewById(R.id.levelWebViewContainer);
//                footerText = (TextView) view.findViewById(R.id.levelControl);
//                fatText = (TextView) view.findViewById(R.id.levelControlFat);
                urlLink = (TextView) view.findViewById(R.id.levelURLAddress);
                break;
            default:
                mListViewer = (ListView) view.findViewById(R.id.fragmentLevelListView);
                mItemSelectedView = (ImageView) view.findViewById(R.id.toolbarItemIconViewSelected);
                mItemSelectedView.setVisibility(View.VISIBLE);
        }

        View viewLevels = view.findViewById(R.id.toolbarLevelsView);
        View viewDetails = view.findViewById(R.id.toolbarDetailsView);
        View toolbarBackButtonView = view.findViewById(R.id.toolbarButtonView);
        ImageView houseIcon = (ImageView) view.findViewById(R.id.toolbarHouseButton);
        ImageView menuIconIdentify = (ImageView) view.findViewById(R.id.toolbarItemIconViewSelected);
        mViewArabikLabel = (TextView) view.findViewById(R.id.toolbarDetailMenuArabicText);
        mViewEnglishLabel = (TextView) view.findViewById(R.id.toolbarDetailMenuEngText);

        viewDetails.setVisibility(View.VISIBLE);
        viewLevels.setVisibility(View.GONE);
        houseIcon.setVisibility(View.GONE);

        if(Preferences.readStringInformation(getContext()
                , Dealer.NAVIGATION_LEVEL_ARB_NAME).equals(Main.LIKES)){
            mViewArabikLabel.setText(getString(R.string.textFavoriteArabic_Details));
            mViewEnglishLabel.setText(getString(R.string.textFavoriteEnglish_Details));
            menuIconIdentify.setImageDrawable(getResources()
                    .getDrawable(R.drawable.toolbar_base_favorite_icon));
        }else {
            mViewArabikLabel.setText(Preferences.readStringInformation(getContext()
                    , Dealer.NAVIGATION_LEVEL_ARB_NAME));
            mViewEnglishLabel.setText(Preferences.readStringInformation(getContext()
                    , Dealer.NAVIGATION_LEVEL_ENG_NAME));
            menuIconIdentify.setImageBitmap(Dealer.readImageView(getContext()
                    , Preferences.readStringInformation(getContext()
                            , Dealer.NAVIGATION_LEVEL_IMG_NAME)));
        }

        toolbarBackButtonView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                assert mListener != null;
                if(Preferences.readStringInformation(getContext()
                        , Dealer.NAVIGATION_LEVEL_ARB_NAME).equals(Main.LIKES))
                    mListener.onStartMenuItemListener(Main.TYPE_BASICS);
                else
                    mListener.onStartMenuItemListener(Main.TYPE_LEVEL);
            }
        });
    }

    public void dialogConfirm(){
        mLevelAboutContiner.setVisibility(View.GONE);
        mSiteOpenContainer.setVisibility(View.VISIBLE);
        txtSiteOpenContainer.setVisibility(View.VISIBLE);
        mSiteOpenContainer.getSettings().setJavaScriptEnabled(true);
        mSiteOpenContainer.loadUrl("http://" + mLinkURL);
    }

    public static DetailsMenu createFragment(){
        return new DetailsMenu();
    }

    public interface OnDetailsMenuListener {
        void onStartMenuItemListener(final int fragmentType);
        void onListenerSearchSaving(HashMap map, String search);
        HashMap<String, ArrayList<String>> onListenerSearchResult();
        void onListenerSaveActivePosition(int positionSelectedInList);
        void onListenerDialogStart();
    }
}
