package com.ploosetech.singsing.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ploosetech.singsing.R;
import com.ploosetech.singsing.activites.Main;
import com.ploosetech.singsing.adapters.BaseMenuAdapter;
import com.ploosetech.singsing.helpers.Animation;

/**
 * Created by Serhii Polishchuk (PoSoH)
 */
public class MessageMenu extends Fragment {

    private OnMessageMenuListener mListener;

    private EditText userName;
    private EditText userPhone;
    private EditText userSubject;
    private EditText userMessage;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layoutFragment = inflater.inflate(R.layout.view_frg_message, container, false);
        initializeFragmentView(layoutFragment);
        return layoutFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof OnMessageMenuListener)
            mListener = (OnMessageMenuListener) context;
        else
            throw new ClassCastException("Error!. Please implements OnMessageMenuListener interface");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void initializeFragmentView(View view){

        userName = (EditText) view.findViewById(R.id.messageUserName);
        userPhone = (EditText) view.findViewById(R.id.messageUserPhone);
        userSubject = (EditText) view.findViewById(R.id.messageUserSubject);
        userMessage = (EditText) view.findViewById(R.id.messageUserMessagee);

        ImageView toolbarBackButton = (ImageView) view.findViewById(R.id.toolbarBackButton);
        toolbarBackButton.setImageDrawable(getResources().getDrawable(R.drawable.arrow_down_button));
        ImageView toolbarHouseButton = (ImageView) view.findViewById(R.id.toolbarHouseButton);
        toolbarHouseButton.setImageDrawable(getResources().getDrawable(R.drawable.toolbar_base_contact_icon));

        View backButton = view.findViewById(R.id.toolbarButtonView);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mListener!=null)mListener.onStartMenuItemListener(Main.TYPE_BASICS);
            }
        });

        View hideView = view.findViewById(R.id.toolbarLevelsView);
        hideView.setVisibility(View.GONE);
        View showView = view.findViewById(R.id.toolbarDetailsView);
        showView.setVisibility(View.VISIBLE);

        ImageView toolbarIconMenuView = (ImageView) view.findViewById(R.id.toolbarItemIconViewSelected);
        toolbarIconMenuView.setVisibility(View.GONE);

        TextView arbText = (TextView) view.findViewById(R.id.toolbarDetailMenuArabicText);
        arbText.setText(getString(R.string.messageBlockArabic_Contact));
        TextView engText = (TextView) view.findViewById(R.id.toolbarDetailMenuEngText);
        engText.setText(getString(R.string.messageBlockEnglish_Contact));

        final ImageView messageError = (ImageView) view.findViewById(R.id.messageErrorView);

        View sendButton = view.findViewById(R.id.massageSendButton);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                messageError.setVisibility(View.GONE);

                if(userName.getText() != null && !userName.getText().toString().equals("")){
                    if(userPhone.getText() != null && !userPhone.getText().toString().equals("")){
                        if(userSubject.getText() != null && !userSubject.getText().toString().equals("")){
                            emailSendQuestion(new MessageSendCompleted() {
                                @Override
                                public void completed() {
                                    assert mListener != null;
                                    mListener.onStartMenuItemListener(Main.TYPE_BASICS);
                                }
                            });

                        }else{
                            messageError.setVisibility(View.VISIBLE);
                            messageError.setImageDrawable(getResources().getDrawable(R.drawable.empty_subject));
                        }
                    }else {
                        messageError.setVisibility(View.VISIBLE);
                        messageError.setImageDrawable(getResources().getDrawable(R.drawable.empty_phone));
                    }
                }else{
                    messageError.setVisibility(View.VISIBLE);
                    messageError.setImageDrawable(getResources().getDrawable(R.drawable.empty_name));
                }
            }
        });
    }

    public static MessageMenu createFragment() {return new MessageMenu();}

    public interface OnMessageMenuListener {
        void onStartMenuItemListener(final int fragmentType);
    }

    public void emailSendQuestion(MessageSendCompleted completed){

        String to = "ramsah.ae@gmail.com";

        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{to});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, userSubject.getText().toString());
        emailIntent.putExtra(Intent.EXTRA_TEXT,
                Html.fromHtml(
                        "<p>"+ userMessage.getText().toString() +"</p>" +
                                "<p>From: " + userName.getText().toString() + "</p>" +
                                "<p>Phone: " + userPhone.getText().toString() + "</p>" ));
        emailIntent.setType("application/octet-stream");
        getContext().startActivity(Intent.createChooser(emailIntent, "Send Email"));
        completed.completed();
    }

    private interface MessageSendCompleted {
        void completed();
    }

}
