package com.ploosetech.singsing.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.ploosetech.singsing.R;
import com.ploosetech.singsing.activites.Main;
import com.ploosetech.singsing.adapters.BaseMenuAdapter;
import com.ploosetech.singsing.helpers.Dealer;
import com.ploosetech.singsing.helpers.Preferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Serhii Polishchuk (PoSoH)
 */
public class LevelMenu extends Fragment {

    private ListView mListView;
    private TextView mEnglishLabel;
    private TextView mArabicLabel;

    private OnLevelMenuListener mListener;

    private int mBaseSelectedPosition = -1;

    private String listItemMenuKeys[] = null;
    private String listItemMenuEng[] = null;
    private String listItemMenuArabic[] = null;
    private String listItemMenuIcons[] = null;
    private String listIcons[] = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layoutFragment = inflater.inflate(R.layout.view_frg_levels, container, false);
        initializeFragmentView(layoutFragment);
        return layoutFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof OnLevelMenuListener)
            mListener = (OnLevelMenuListener) context;
        else
            throw new ClassCastException("Error!. Please implements OnLevelMenuListener interface");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        String KEY = Preferences.readStringInformation(getContext(),Dealer.NAVIGATION_LEVEL);
        JSONObject menuSelected;
        String JSON = getString(R.string.BasedLevels);

        try {
            menuSelected = new JSONObject(JSON);
            JSONArray english = menuSelected.getJSONObject(KEY).getJSONArray("E");
            JSONArray arabic = menuSelected.getJSONObject(KEY).getJSONArray("A");
            JSONArray images = menuSelected.getJSONObject(KEY).getJSONArray("IMAGES");
            JSONArray icons = menuSelected.getJSONObject(KEY).getJSONArray("ICONS");
            JSONArray keys = menuSelected.getJSONObject(KEY).getJSONArray("KEYS");

            listItemMenuEng = new String[english.length()];
            listItemMenuArabic = new String[arabic.length()];
            listItemMenuIcons = new String[images.length()];
            listItemMenuKeys = new String[keys.length()];
            listIcons = new String[icons.length()];

            for(int pos=0, l=english.length();pos<l;pos++){
                listItemMenuEng[pos] = english.getString(pos);
            }
            for(int pos=0, l=arabic.length();pos<l;pos++){
                listItemMenuArabic[pos] = arabic.getString(pos);
            }
            for(int pos=0, l=images.length();pos<l;pos++){
                listItemMenuIcons[pos] = images.getString(pos);
            }
            for(int pos=0, l=icons.length();pos<l;pos++){
                listIcons[pos] = icons.getString(pos);
            }
            for(int pos=0, l=keys.length();pos<l;pos++){
                listItemMenuKeys[pos] = keys.getString(pos);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        BaseMenuAdapter adapter = new BaseMenuAdapter(getContext(),
                listItemMenuEng,
                listItemMenuArabic,
                listItemMenuIcons);
        mListView.setAdapter(adapter);

        final String[] finalListItemMenuArabic = listItemMenuArabic;
        final String[] finalListItemMenuEng = listItemMenuEng;

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(mListener != null){
                    assert finalListItemMenuArabic != null;
                    Preferences.saveStringInformation(getContext(), Dealer.NAVIGATION_DETAILS, listItemMenuKeys[i]);

                    Preferences.saveStringInformation(getContext(), Dealer.NAVIGATION_LEVEL_ENG_NAME, listItemMenuEng[i]);
                    Preferences.saveStringInformation(getContext(), Dealer.NAVIGATION_LEVEL_ARB_NAME, listItemMenuArabic[i]);
                    Preferences.saveStringInformation(getContext(), Dealer.NAVIGATION_LEVEL_IMG_NAME, listIcons[i]);

                    mListener.onStartMenuItemListener(Main.TYPE_DETAILS);
                }
            }
        });

        mArabicLabel.setText(Preferences.readStringInformation(getContext(), Dealer.NAVIGATION_BASE_ARB_NAME));
        mEnglishLabel.setText(Preferences.readStringInformation(getContext(), Dealer.NAVIGATION_BASE_ENG_NAME));
    }

    private void initializeFragmentView(View view){
        mListView = (ListView) view.findViewById(R.id.fragmentLevelListView);

        mArabicLabel = (TextView) view.findViewById(R.id.toolbarLevelMenuArabicText);
        mEnglishLabel = (TextView) view.findViewById(R.id.toolbarLevelMenuEngText);

        View backButton = view.findViewById(R.id.toolbarButtonView);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mListener !=null)
                    mListener.onStartMenuItemListener(Main.TYPE_BASICS);
            }
        });
    }

    public static LevelMenu createFragment() {
        return new LevelMenu();
    }

    public void setInformationToFragment(final int position){
        this.mBaseSelectedPosition = position;
    }

    public interface OnLevelMenuListener {
        void onStartMenuItemListener(final int fragmentType);
    }
}
