package com.ploosetech.singsing.fragments;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.ploosetech.singsing.R;
import com.ploosetech.singsing.activites.Main;
import com.ploosetech.singsing.adapters.BaseMenuAdapter;
import com.ploosetech.singsing.adapters.DetailsMenuAdapter;
import com.ploosetech.singsing.helpers.Dealer;
import com.ploosetech.singsing.helpers.Preferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Serhii Polishchuk (PoSoH)
 */
public class BasicsMenu extends Fragment {

    private OnBaseMenuListener mListener;
    private String keysList[] = null;
    private String englishTexts[] = null;
    private String arabicTexts[] = null;
    private String iconsLink[] = null;

    private ImageView searchButtonEnable; // = (ImageView) view.findViewById(R.id.basicsSearchButtonEnable);
    private ImageView searchButtonStart; // = (ImageView) view.findViewById(R.id.basicsSearchButtonStart);
    private ImageView closeSearchButton; // = (ImageView) view.findViewById(R.id.basicsCloseSearchButton);
    private ImageView mButtonLikesView;

    private ListView mList;
    private ListView mListSearch;
    private EditText mSearchTextView;

    private HashMap<String, ArrayList<String>> information = new HashMap<String, ArrayList<String>>();
    private DetailsMenuAdapter mDetailsAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layoutFragment = inflater.inflate(R.layout.view_frg_basics, container, false);
        initializeFragmentView(layoutFragment);
        return layoutFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof OnBaseMenuListener)
            mListener = (OnBaseMenuListener) context;
        else
            throw new ClassCastException("Error!. Please implements OnBaseMenuListener interface");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void initializeFragmentView(View view){

        JSONObject menuSelected;
        String JSON = getString(R.string.menu);
        try {
            menuSelected = new JSONObject(JSON);
            JSONArray english = menuSelected.getJSONArray("MENU_E");
            JSONArray arabic = menuSelected.getJSONArray("MENU_A");
            JSONArray icons = menuSelected.getJSONArray("IMAGES");
            JSONArray keys = menuSelected.getJSONArray("KEY");
            englishTexts = new String[english.length()];
            arabicTexts = new String[arabic.length()];
            iconsLink = new String[icons.length()];
            keysList = new String[keys.length()];

            for(int pos=0, l=english.length();pos<l;pos++){
                englishTexts[pos] = english.getString(pos);
            }
            for(int pos=0, l=arabic.length();pos<l;pos++){
                arabicTexts[pos] = arabic.getString(pos);
            }
            for(int pos=0, l=icons.length();pos<l;pos++){
                iconsLink[pos] = icons.getString(pos);
            }
            for(int pos=0, l=keys.length();pos<l;pos++){
                keysList[pos] = keys.getString(pos);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        mList = (ListView) view.findViewById(R.id.fragmentBasicsListView);
        final BaseMenuAdapter adapter = new BaseMenuAdapter(getContext(),
                englishTexts,
                arabicTexts,
                iconsLink);

        mList.setAdapter(adapter);

        mListSearch = (ListView) view.findViewById(R.id.fragmentBasicsListViewSearch);

        mSearchTextView = (EditText) view.findViewById(R.id.fragmentBasicInputSearchField);
        mSearchTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {

                if (!TextUtils.isEmpty(s)) {
//                    mSearchTextView.setEnabled(false);
                    Dealer.collectionSearch(getContext()
                            , s.toString(), new Dealer.OnResultSearch() {
                                @Override
                                public void resultSuccess(ArrayList<String> engResult
                                        , ArrayList<String> arbResult
                                        , ArrayList<String> uaeResult
                                        , ArrayList<String> vidResult) {

//                                    mSearchTextView.setEnabled(true);

                                    information.put(Main.TYPE_TEXT_ENG, engResult);
                                    information.put(Main.TYPE_TEXT_ARB, arbResult);
                                    information.put(Main.TYPE_TEXT_UAE, uaeResult);
                                    information.put(Main.TYPE_TEXT_VID, vidResult);

                                    if(mListener!=null)
                                        mListener.onListenerSearchSaving(information
                                                , mSearchTextView.getText().toString());

                                    mList.setVisibility(View.GONE);
                                    mListSearch.setVisibility(View.VISIBLE);
                                    mDetailsAdapter = new DetailsMenuAdapter(getContext(),
                                            information.get(Main.TYPE_TEXT_ENG),
                                            information.get(Main.TYPE_TEXT_ARB));

                                    mListSearch.setAdapter(mDetailsAdapter);
                                    mListSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                        @Override
                                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                            Dealer.LOG("ITEM -> " + i);
                                            assert mListener!=null;
                                            mListener.onListenerSaveActivePosition(i);
                                            mListener.onStartMenuItemListener(Main.TYPE_CONTENT);
                                        }
                                    });
                                }
                            });
                } else {

                    if(mListener!=null) {
                        mListener.onListenerChangeSearchMode(false);
                        mListener.onListenerSearchSaving(new HashMap()
                                    , "");
                    }

                    mListSearch.setVisibility(View.GONE);
                    mList.setVisibility(View.VISIBLE);
                    Dealer.hideKeyboard(getActivity());
                }
            }
        });

        searchButtonEnable = (ImageView) view.findViewById(R.id.basicsSearchButtonEnable);
        searchButtonStart = (ImageView) view.findViewById(R.id.basicsSearchButtonStart);
        closeSearchButton = (ImageView) view.findViewById(R.id.basicsCloseSearchButton);

        searchButtonEnable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(mListener!=null)
                    mListener.onListenerChangeSearchMode(true);

                enableSearchMode();

            }
        });

        searchButtonStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        closeSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchButtonEnable.setVisibility(View.VISIBLE);
                closeSearchButton.setVisibility(View.INVISIBLE);
                mSearchTextView.setVisibility(View.INVISIBLE);
                searchButtonStart.setVisibility(View.INVISIBLE);

                mListSearch.setVisibility(View.GONE);
                mList.setVisibility(View.VISIBLE);

                assert mListener != null;
                mListener.onListenerSearchSaving(new HashMap(), "");
                mSearchTextView.setText("");
                Dealer.hideKeyboard(getActivity());
            }
        });

        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(mListener != null){
                    Preferences.saveStringInformation(getContext(), Dealer.NAVIGATION_LEVEL, keysList[i]);
                    Preferences.saveStringInformation(getContext(), Dealer.NAVIGATION_BASE_ENG_NAME, englishTexts[i]);
                    Preferences.saveStringInformation(getContext(), Dealer.NAVIGATION_BASE_ARB_NAME, arabicTexts[i]);
                    mListener.onStartMenuItemListener(Main.TYPE_LEVEL);
                }
            }
        });

        ImageView messageButton = (ImageView) view.findViewById(R.id.toolbarContactButton);
        messageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mListener!=null)mListener.onStartMenuItemListener(Main.TYPE_MESSAGE);
            }
        });

        mButtonLikesView = (ImageView) view.findViewById(R.id.toolbarLikeButton);
        mButtonLikesView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dealer.collectionSearch(getContext(), null, new Dealer.OnResultSearch() {
                    @Override
                    public void resultSuccess(ArrayList<String> engResult
                            , ArrayList<String> arbResult
                            , ArrayList<String> uaeResult
                            , ArrayList<String> videoResult) {

                        ArrayList<String> favorites = Preferences.readListInformation(getContext()
                                , Preferences.TYPE_LIKE);

                        ArrayList<String> favEng = new ArrayList<String>();
                        ArrayList<String> favArb = new ArrayList<String>();
                        ArrayList<String> favUAE = new ArrayList<String>();
                        ArrayList<String> favVid = new ArrayList<String>();

                        for(int pos=0, len=engResult.size(); pos<len;pos++){
                            for(String like : favorites) {
                                if (engResult.get(pos).equals(like)) {
                                    favEng.add(engResult.get(pos));
                                    favArb.add(arbResult.get(pos));
                                    favUAE.add(uaeResult.get(pos));
                                    favVid.add(videoResult.get(pos));
                                }
                            }
                        }

                        information.put(Main.TYPE_TEXT_ENG, favEng);
                        information.put(Main.TYPE_TEXT_ARB, favArb);
                        information.put(Main.TYPE_TEXT_UAE, favUAE);
                        information.put(Main.TYPE_TEXT_VID, favVid);

                        Preferences.saveStringInformation(getContext(), Dealer.NAVIGATION_LEVEL_ENG_NAME, Main.LIKES);
                        Preferences.saveStringInformation(getContext(), Dealer.NAVIGATION_LEVEL_ARB_NAME, Main.LIKES);
                        Preferences.saveStringInformation(getContext(), Dealer.NAVIGATION_LEVEL_IMG_NAME, Main.LIKES);

                        Preferences.saveStringInformation(getContext(), Dealer.NAVIGATION_DETAILS, Main.LIKES);

                        assert mListener != null;
                        mListener.onListenerChangeSearchMode(false);
                        mListener.onListenerSearchSaving(information
                                , null);
                        mListener.onStartMenuItemListener(Main.TYPE_DETAILS);

                    }
                });
            }
        });

        assert mListener != null;
        if(mListener.onListenerSearches()){
            enableSearchMode();

            mSearchTextView.setText(mListener.onListenerSearchString());

            information = mListener.onListenerSearchResult();

            mList.setVisibility(View.GONE);
            mListSearch.setVisibility(View.VISIBLE);
            mDetailsAdapter = new DetailsMenuAdapter(getContext(),
                    information.get(Main.TYPE_TEXT_ENG),
                    information.get(Main.TYPE_TEXT_ARB));

            mListSearch.setAdapter(mDetailsAdapter);
            mListSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Dealer.LOG("ITEM -> " + i);
                    if(mListener!=null)
                        mListener.onStartMenuItemListener(Main.TYPE_CONTENT);
                }
            });
        }
    }

    private void enableSearchMode(){
        searchButtonEnable.setVisibility(View.GONE);
        closeSearchButton.setVisibility(View.VISIBLE);
        mSearchTextView.setVisibility(View.VISIBLE);
        searchButtonStart.setVisibility(View.VISIBLE);
    }

    public static BasicsMenu createFragment() {return new BasicsMenu();}

    public interface OnBaseMenuListener {
        void onStartMenuItemListener(final int fragmentType);
        void onListenerChangeSearchMode(boolean status);
        String onListenerSearchString();
        HashMap<String, ArrayList<String>> onListenerSearchResult();
        void onListenerSearchSaving(HashMap map, String search);
        boolean onListenerSearches();
        void onListenerSaveActivePosition(int positionSelectedInList);
    }
}
