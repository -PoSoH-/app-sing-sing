package com.ploosetech.singsing.adapters;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ploosetech.singsing.R;
import com.squareup.picasso.Picasso;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Serhii Polishchuk (PoSoH)
 */
public class BaseMenuAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private String []englishText;
    private String []arabicText;
    private String []icons;
    private Context context;

    public BaseMenuAdapter(Context context, String []englishTexts, String []arabicTexts, String []icons){ //, ChatAdapterListener listener){ //, String _ObjectId, String _MyObjectID){
        this.context = context;
        this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.englishText = englishTexts;
        this.arabicText = arabicTexts;
        this.icons = icons;
    }

    @Override
    public int getCount() {
        return englishText.length;
    }

    @Override
    public Object getItem(int position) {
        return englishText[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null) {
            convertView = this.inflater.inflate(R.layout.view_item_menu, null, false);
        }

        ((TextView)convertView.findViewById(R.id.itemListMenuEngText)).setText(englishText[position]);
        ((TextView)convertView.findViewById(R.id.itemListMenuArabicText)).
                setText(arabicText[position]);
        ImageView resource = (ImageView)convertView.findViewById(R.id.itemListLogoName);

        InputStream stream = null;
        try {
            AssetManager manager = this.context.getAssets();
            stream = manager.open(icons[position]);
            resource.setImageBitmap(BitmapFactory.decodeStream(stream));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return convertView;
    }

//    public String getSelectedKey(int position){
//
//        return
//    }
}
