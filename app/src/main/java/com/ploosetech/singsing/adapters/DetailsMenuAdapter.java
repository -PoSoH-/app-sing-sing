package com.ploosetech.singsing.adapters;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ploosetech.singsing.R;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
/**
 * Created by Serhii Polishchuk (PoSoH)
 */
public class DetailsMenuAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private List<String>englishText;
    private List<String>arabicText;
    private Context context;

    public DetailsMenuAdapter(Context context, List<String>englishTexts, List<String> arabicTexts){ //, ChatAdapterListener listener){ //, String _ObjectId, String _MyObjectID){
        this.context = context;
        this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.englishText = englishTexts;
        this.arabicText = arabicTexts;
    }

    @Override
    public int getCount() {
        return englishText.size();
    }

    @Override
    public Object getItem(int position) {
        return englishText.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null) {
            convertView = this.inflater.inflate(R.layout.view_item_details_menu, null, false);
        }

        ((TextView)convertView.findViewById(R.id.itemListMenuEngText)).setText(englishText.get(position));
        ((TextView)convertView.findViewById(R.id.itemListMenuArabicText)).
                setText(arabicText.get(position));

        return convertView;
    }
}
