package com.ploosetech.singsing.activites;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.ploosetech.singsing.R;
import com.ploosetech.singsing.fragments.*;
import com.squareup.picasso.Picasso;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Sergiy Polishuk (PoSoH) on 02.08.2016.
 */
public class Splash extends AppCompatActivity {

    private BroadcastReceiver splashReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Main.showView(getApplicationContext());
            Splash.this.finish();
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_atv_splash);

        registerReceiver(splashReceiver, new IntentFilter("RECEIVER_SPLASH"));
        ImageView imageView = (ImageView) findViewById(R.id.backgroundSplashScreen);
        Picasso.with(getApplicationContext()).load(R.drawable.splash).fit().into(imageView);

        new AsyncTask<Void, Void, Void>(){

            @Override
            protected Void doInBackground(Void... voids) {
                Timer timer = new Timer("PAUSE");
                final int milliseconds = getResources().getInteger(R.integer.pause_milliseconds);
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
//                        try {
//                            Thread.sleep(milliseconds);
                            Intent intent = new Intent("RECEIVER_SPLASH");
                            sendBroadcast(intent);
//                        } catch (InterruptedException e) {
//                            e.printStackTrace();
//                        }
                    }
                }, milliseconds);
                return null;
            }
        }.execute();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(splashReceiver);
    }
}
