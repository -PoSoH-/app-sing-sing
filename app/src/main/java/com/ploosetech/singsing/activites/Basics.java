package com.ploosetech.singsing.activites;

import android.app.Activity;
import android.support.annotation.LayoutRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;

import com.ploosetech.singsing.R;

/**
 * Created by Serhii Polishchuk (PoSoH) on 02.08.2016.
 */
public class Basics extends AppCompatActivity {

//    private RelativeLayout mBasicsToolbar;
    private FrameLayout mBasicsContent;

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        setContentView(getLayoutInflater().inflate(layoutResID, null));
    }

    @Override
    public void setContentView(View view) {
        FrameLayout fullBasicsLayout = (FrameLayout) getLayoutInflater().inflate(R.layout.view_atv_base, null);
        mBasicsContent = (FrameLayout) fullBasicsLayout.findViewById(R.id.basicsContentView);
        mBasicsContent.addView(view);
        super.setContentView(fullBasicsLayout);
    }


    public void setFragment(Fragment fragment, int layoutId){
        FragmentManager fragmentManager = getSupportFragmentManager();
        String tag = fragment.getClass().getSimpleName();
        if (fragmentManager.findFragmentByTag(tag) == null)
            fragmentManager.beginTransaction().replace(layoutId, fragment, tag).commit();
    }

    public static final int RIGHT_ANIMATION = -5; // animation is right to left
    public static final int LEFT_ANIMATION = -6; // animation is left to right
    public static final int DOWN_ANIMATION = -7; // animation is down to up
    public static final int UP_ANIMATION = -8; // animation is up to down

    public static void setFragmentAnimation(FragmentManager fragmentManager, Fragment fragment,
                                            int layoutResIs, boolean addToBackStack, int typeAnimation){
        String tag = fragment.getClass().getSimpleName();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        switch (typeAnimation) {
            case RIGHT_ANIMATION:
                fragmentTransaction.setCustomAnimations(
                        R.anim.slide_right_to_left_new_fr
                        , R.anim.slide_right_to_left_old_fr
                );
                break;
            case LEFT_ANIMATION:
                fragmentTransaction.setCustomAnimations(
                        R.anim.slide_left_to_rigth_new_fr
                        , R.anim.slide_left_to_right_old_fr
                );
                break;
            case UP_ANIMATION:
                fragmentTransaction.setCustomAnimations(
                        R.anim.slide_down_to_center_fr
                        , R.anim.slide_center_to_up_fr
                );
                break;
            case DOWN_ANIMATION:
                fragmentTransaction.setCustomAnimations(
                        R.anim.slide_center_to_down_fr
                        , R.anim.slide_up_to_center_fr
                );
                break;
        }
        fragmentTransaction.replace(layoutResIs, fragment, tag);
        if (addToBackStack) fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public Fragment getFragment(Class cl){
        return getSupportFragmentManager().findFragmentByTag(cl.getSimpleName());
    }

    public static Basics get(Activity activity){
        return (Basics) activity;
    }

    public void onBackPressedWithAnimation(){
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_start_enter_left_side, R.anim.activity_start_exit_left_side);
    }

    public static void animationRightToLeft(Activity activity){
        activity.overridePendingTransition(R.anim.activity_start_enter_right_side,
                R.anim.activity_start_exit_right_side);
    }

    public static void setRightAnimation(Activity activity){
        activity.overridePendingTransition(R.anim.activity_start_enter_right_side,
                R.anim.activity_start_exit_right_side);
    }

}
