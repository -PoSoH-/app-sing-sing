package com.ploosetech.singsing.activites;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.widget.FrameLayout;

import com.ploosetech.singsing.R;
import com.ploosetech.singsing.dialogs.Confirmation;
import com.ploosetech.singsing.fragments.BasicsMenu;
import com.ploosetech.singsing.fragments.ContentMenu;
import com.ploosetech.singsing.fragments.DetailsMenu;
import com.ploosetech.singsing.fragments.LevelMenu;
import com.ploosetech.singsing.fragments.MessageMenu;
import com.ploosetech.singsing.helpers.Dealer;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Serhii Polishchuk (PoSoH) on 02.08.2016.
 */
public class Main extends Basics implements BasicsMenu.OnBaseMenuListener
        , LevelMenu.OnLevelMenuListener
        , DetailsMenu.OnDetailsMenuListener
        , ContentMenu.OnContentMenuListener
        , MessageMenu.OnMessageMenuListener
        , Confirmation.OnExitListener {

    public static final int TYPE_BASICS = 105;
    public static final int TYPE_LEVEL = 107;
    public static final int TYPE_DETAILS = 109;
    public static final int TYPE_CONTENT = 111;
    public static final int TYPE_MESSAGE = 205;
    public static final int TYPE_LIKES = 113;

    public static final String KEY_POSITION_MENU = "KEY.POSITION.MENU";

    private int mCurrentFragment = -1;

    private int mCurrentContainer = 0;

    private HashMap<String, ArrayList<String>> mBaseSearchResult = new HashMap<>();
    private String mTextSearch;
    public static final String LIKES = "FAVORITES";
    public static final String TYPE_TEXT_ENG = "ENG";
    public static final String TYPE_TEXT_ARB = "ARB";
    public static final String TYPE_TEXT_UAE = "UAE";
    public static final String TYPE_TEXT_VID = "VID";
    private boolean mSearch = false;
    private boolean mLikes  = false;

    private int mSelectedPosition = -2;

    private Confirmation mConfirmationDialog;

    private FrameLayout mBasicsContainer;

//    private EditText textSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_atv_main);
        mCurrentContainer = R.id.mainBasicsContainer;
//        mBasicsContainer = (FrameLayout) findViewById(R.id.mainBasicsContainer);
        managerFragmentsView(TYPE_BASICS);
    }

    private void managerFragmentsView(final int fragmentType) { //}, final int position){
        Fragment fragment = null;
        switch (fragmentType){
            case TYPE_BASICS:
                fragment = BasicsMenu.createFragment();
                break;
            case TYPE_LEVEL:
                fragment = LevelMenu.createFragment();
//                ((LevelMenu)fragment).setInformationToFragment();
                break;
            case TYPE_DETAILS:
                fragment = DetailsMenu.createFragment();
//                ((DetailsMenu)fragment).setInformationToFragment();
                break;
            case TYPE_CONTENT:
                fragment = ContentMenu.createFragment();
                break;
            case TYPE_MESSAGE:
                fragment = MessageMenu.createFragment();
                break;
            case TYPE_LIKES:
                fragment = MessageMenu.createFragment();
                break;
        }
        if(fragment != null){
            if(fragmentType > 200 || mCurrentFragment > 200) {
                if(fragmentType > mCurrentFragment)
                    setFragmentAnimation(getSupportFragmentManager(), fragment, mCurrentContainer, false, Basics.UP_ANIMATION);
                else
                    setFragmentAnimation(getSupportFragmentManager(), fragment, mCurrentContainer, false, Basics.DOWN_ANIMATION);
            }else{
                if(fragmentType > mCurrentFragment)
                    setFragmentAnimation(getSupportFragmentManager(), fragment, mCurrentContainer, false, Basics.RIGHT_ANIMATION);
                else
                    setFragmentAnimation(getSupportFragmentManager(), fragment, mCurrentContainer, false, Basics.LEFT_ANIMATION);
            }
            mCurrentFragment = fragmentType;
        }
    }

    public static void showView(Context context){
        Intent intent = new Intent(context, Main.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    // implements realization interface

    @Override
    public void onStartMenuItemListener(final int fragmentType) {
        Dealer.hideKeyboard(this);
//        if(!mLikes) {
//            if (fragmentType == TYPE_DETAILS && mCurrentFragment == TYPE_BASICS) {
//                mLikes = true;
//            }
//        }else {
//            if (fragmentType == TYPE_BASICS && mCurrentFragment == TYPE_DETAILS) {
//                mLikes = false;
//            }
//        }
        managerFragmentsView(fragmentType);
    }

    @Override
    public void onListenerChangeSearchMode(boolean status) {
        mSearch = status;
    }

//    @Override
//    public boolean onListenerLikes() {
//        return mLikes;
//    }

    @Override
    public String onListenerSearchString() {
        return mTextSearch;
    }

    @Override
    public HashMap<String, ArrayList<String>> onListenerSearchResult() {
        return mBaseSearchResult;
    }

    @Override
    public boolean onListenerSearches() {
        return mSearch;
    }

    @Override
    public int onListenerActivePosition() {
        return this.mSelectedPosition;
    }

    @Override
    public void onListenerSaveActivePosition(int positionSelectedInList) {
        this.mSelectedPosition = positionSelectedInList;
    }

    @Override
    public void onListenerDialogStart() {
        mConfirmationDialog = new Confirmation();
        mConfirmationDialog.show(getSupportFragmentManager(), "DIALOG");
    }

    @Override
    public void onListenerSearchSaving(HashMap map, String search) {
        this.mTextSearch = search;
        this.mBaseSearchResult = map;
    }

    @Override
    public void onConfirmDialog(boolean status) {
        mConfirmationDialog.dismiss();
        if(status) {
            Fragment fragment = getSupportFragmentManager().findFragmentById(mCurrentContainer);
            if(fragment != null && fragment instanceof DetailsMenu){
                ((DetailsMenu)fragment).dialogConfirm();
            }
        }
    }

//    @Override
//    public void onEditableListener(final EditText textSearch, final Dealer.OnResultSearch result) {
//        this.textSearch = textSearch;
//        this.textSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                String searchString = v.getText().toString();
//                if (actionId == EditorInfo.IME_ACTION_SEARCH && !TextUtils.isEmpty(searchString)) {
//                    Dealer.collectionSearch(getApplicationContext()
//                            , searchString.toString(), new Dealer.OnResultSearch() {
//                        @Override
//                        public void resultSuccess(ArrayList<String> engResult, ArrayList<String> arbResult) {
//                            result.resultSuccess(engResult, arbResult);
//                        }
//                    });
//
//                    return true;
//                } else {
//                    return false;
//                }
//            }
//        });
//    }

//    @Override
//    public void onStartMenuItemListener(final String fragmentType, final int position) {
//        managerFragmentsView(fragmentType, position);
//    }
//
//    @Override
//    public void onStartMenuItemListener(String fragmentType, int position, String arabicName, String englishName) {
//        managerFragmentsView(fragmentType, position);
//    }
//
//    @Override
//    public void onStartMenuItemListener(String fragmentType, int advancePosition, int itemViewPosition, String itemArabicName, String itemEnglishName) {
//        managerFragmentsView(fragmentType, itemViewPosition);
//    }

}
