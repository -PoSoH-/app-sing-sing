package com.ploosetech.singsing.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.ploosetech.singsing.R;

/**
 * Created by Serhii Polishchuk (PoSoH)
 */
public class Confirmation extends DialogFragment {

    public static final int EXIT_DIALOG = -100;
    public static final int LOG_OUT_DIALOG = -110;

    private OnExitListener mListener;
    private Confirmation mConfirmation;
    private FragmentTransaction ft = null;
    private int type = -1;
    private String typeString = null;
//    private DeliveryConfirmation dialog = null;

//    public void show(FragmentActivity activity, String classNameFragment, int typeMessage) {
//        mConfirmation = new Confirmation();
//        ft = activity.getSupportFragmentManager().beginTransaction();
//        activity.getIntent().putExtra("CONFIRM_DIALOG_TYPE", classNameFragment);
//        activity.getIntent().putExtra("CONFIRM_DIALOG_MESSAGE", typeMessage);
//        ft.add(mConfirmation, Dialog.class.getSimpleName());
//        ft.commit();
//    }

//    public void exitDialog(){ mConfirmation.dismiss();}

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof OnExitListener) {
            mListener = (OnExitListener) activity;
        } else {
            throw new RuntimeException(activity.toString() + " must implements OnSelectedDeliveryListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity());
        final View view = getActivity().getLayoutInflater().inflate(R.layout.view_dalog_confirm,
                null);

        type = getActivity().getIntent().getIntExtra("CONFIRM_DIALOG_TYPE", -1);

        View btnClickConfirm    = view.findViewById(R.id.buttonYes);
        View btnClickConfirmNo = view.findViewById(R.id.buttonNo);

        btnClickConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onConfirmDialog(true);
            }
        });

        btnClickConfirmNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mListener!=null) mListener.onConfirmDialog(false);
            }
        });

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(view);
        return dialog;
    }

    public interface OnExitListener {
        void onConfirmDialog(boolean status);
    }

}
